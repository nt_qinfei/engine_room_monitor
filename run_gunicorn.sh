#!/usr/bin/env bash

# 如果使用 Docker 请不要注释下面这一行
 cd /code
# 由于消息队列的问题，此处只能有一个线程在跑，否则会重复写入数据库消息
gunicorn --workers=1 manage:app -b 0.0.0.0:5000