# 机房监控系统 API

## 系统概述

机房监控系统，包含用户管理、设备管理、日志记录等部分,数据存储在docker mysql中。

系统架构如下图：
![系统架构](http://glrsmart.oss-cn-shanghai.aliyuncs.com/www/webpic/architecture.png)


## app 接口

#### 用户管理模块

用户管理接口 url：/user

用户管理模块使用方法：

    添加用户
    
        使用 post 方法请求，参数：username password 键值对
        
        举例返回：
           {
                "success": 0,
                "msg": "User insert ok!",
                "data": {
                    "username": "test1"
                }
            }
           
           错误结果：
           {
                "success": 1,
                "msg": "User already exists!",
                "data": {
                    "username": "test"
                }
           }
    
    查找用户
    
        使用 get 方法请求，参数：username， 可以确认用户是否存在
        
        举例参照 post 方法
    
用户授权接口 url：/login

用户授权模块使用方法：
    
    使用 post 方法请求， 参数：username password 键值对
    
        举例返回：
        {
            "success": 0,
            "msg": "User authorize success!",
            "data": {
                "token": "eyJ1c2VybmFtZSI6ICJ0ZXN0IiwgImV4cCI6ICIyMDE3LTA3LTIzIDAyOjI5OjAyIn0="
            }
        }
        
        错误结果：
        {
            "success": 1,
            "msg": "User authorize failed!",
            "data": {
                "token": ""
            }
        }
    
    

#### 设备管理模块

设备管理接口 url: /device

查询设备列表 http get方法用来查找设备信息，输出信息遵循 devices_response_marshaller 格式

    
    token: 用户认证令牌 
    
    topic: 设备 topic 可以认为是设备唯一标示, 没有此参数的时候返回用户所有设备列表
    
    
    举例返回：
    
    {
        "success": 0,
        "msg": "Get device list success!",
        "data": [
            {
                "device_name": "tdevice",
                "topic": "er/a",
                "value": "0",
                "remarks": "hhhh"
            }
        ]
    }

添加新设备 http post方法，输出信息遵循 devices_response_marshaller 格式

    token: 用户认证令牌 
    
    topic: 设备 topic 可以认为是设备唯一标示 , 为空则返回所有设备
    
    device_name: 设备名称
    
    remarks: 设备备注信息
    
    举例返回：
    
    {
        "success": 0,
        "msg": "Insert device success!",
        "data": [
            {
                "device_name": "tdevice",
                "topic": "er/a",
                "value": "0",
                "remarks": "hhhh"
            }
        ]
    }
    
    错误信息：
    
    {
        "success": 1,
        "msg": "Topic has been used!",
        "data": []
    }

删除设备 http delete 方法用来删除设备，输出信息遵循 devices_response_marshaller 格式

    token: 用户认证令牌 
    
    topic: 设备 topic 可以认为是设备唯一标示 , 为空则返回所有设备
    
    举例返回：
    
    {
        "success": 0,
        "msg": "Delete device success!",
        "data": []
    }
    
    错误信息：
    {
        "success": 1,
        "msg": "Delete not exists!",
        "data": []
    }

    {
        "success": 1,
        "msg": "Topic can not be null!",
        "data": []
    }
    
#### 设备历史记录模块

设备历史记录接口 url: /log

插入设备历史记录，通过 mqtt 即时通信系统插入，无需 post ，设备向 topic 发布信息，会自动记录在数据库中

查询设备历史记录 http get方法，输出信息遵循 logs_response_marshaller 格式

    token: 用户认证令牌 
    
    topic: 设备 topic 可以认为是设备唯一标示 , 为空则返回所有设备
    
    per: 每页返回的条目数量
    
    page: 请求的 第几页，默认应该填写1

    返回结果：
    {
        "success": 0,
        "msg": "Get device logs success!",
        "page": 2,
        "current_page": 2,
        "data": [
            {
                "value": "025",
                "date_time": "2017-07-12 10:13:43"
            },
            {
                "value": "025",
                "date_time": "2017-07-12 10:13:43"
            },
            {
                "value": "025",
                "date_time": "2017-07-12 10:13:43"
            },
            {
                "value": "025",
                "date_time": "2017-07-12 10:13:43"
            },
            {
                "value": "025",
                "date_time": "2017-07-12 10:13:43"
            },
            {
                "value": "025",
                "date_time": "2017-07-12 10:13:44"
            },
            {
                "value": "025",
                "date_time": "2017-07-12 10:13:44"
            },
            {
                "value": "025",
                "date_time": "2017-07-12 10:13:44"
            },
            {
                "value": "025",
                "date_time": "2017-07-12 10:13:44"
            }
        ]
    }


#### 通信组件（指令、状态）

通信组件使用 mqtt 消息队列，服务器信息在 config.py 中

esp8266 订阅自身 topic 即可开始推送信息和接收到手机 App 发送来的指令信息

手机 App 通过扫码添加设备，并订阅设备 id 作为 topic 可以接受到 esp8266 推送上来的设备信息没解析之后即可实时显示


#### HTTP 状态码说明

* 200 OK - [GET]：服务器成功返回用户请求的数据，该操作是幂等的（Idempotent）。
* 201 CREATED - [POST/PUT/PATCH]：用户新建或修改数据成功。
* 202 Accepted - [*]：表示一个请求已经进入后台排队（异步任务）
* 204 NO CONTENT - [DELETE]：用户删除数据成功。
* 400 INVALID REQUEST - [POST/PUT/PATCH]：用户发出的请求有错误，服务器没有进行新建或修改数据的操作，该操作是幂等的。
* 401 Unauthorized - [*]：表示用户没有权限（令牌、用户名、密码错误）。
* 403 Forbidden - [*] 表示用户得到授权（与401错误相对），但是访问是被禁止的。
* 404 NOT FOUND - [*]：用户发出的请求针对的是不存在的记录，服务器没有进行操作，该操作是幂等的。
* 406 Not Acceptable - [GET]：用户请求的格式不可得（比如用户请求JSON格式，但是只有XML格式）。
* 410 Gone -[GET]：用户请求的资源被永久删除，且不会再得到的。
* 422 Unprocesable entity - [POST/PUT/PATCH] 当创建一个对象时，发生一个验证错误。
* 500 INTERNAL SERVER ERROR - [*]：服务器发生错误，用户将无法判断发出的请求是否成功。

#### 数据库结构说明

数据库结构如下所示：

数据表：

    device
        
    device_log
    
    user

user 表：

    id
    
    username
    
    password_hash
    
device:
    
    id
    
    username
    
    device_name
    
    topic
    
    value
    
    remarks
    
    date_time
    
device_log:
    
    id
    
    topic
    
    value
    
    date_time
