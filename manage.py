# -*- encoding:utf-8 -*-
from app import app
import config
'''
机房监控系统，包含用户管理、设备管理、日志记录等部分
数据存储在docker mysql中。
'''

if __name__ == "__main__":
    '''
    调试的时候运行此文件，python ./manage.py 会运行下面语句，开始运行整个工程
    '''
    app.run(host=config.DEBUG_HOST_ADDR)
