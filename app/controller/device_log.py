import json
import config
import datetime
from flask_restful import Resource, fields, marshal
from flask import request
import paho.mqtt.client as mqtt
from app.controller import Session
from json.decoder import JSONDecodeError
from app.models import device_log as m_log
from app.controller import token_auth

# 预定义了返回数据的格式
logs_marshaller = {
    'value': fields.String,
    'date_time': fields.String
}

# 预定义了返回数据的格式
logs_response_marshaller = {
    'success': fields.Integer,
    'msg': fields.String,
    'page': fields.Integer,
    'current_page': fields.Integer,
    'data': fields.String
}


class DeviceLogResource(Resource):
    def __init__(self):
        self.response_data = self.response_handler([], 1, "Unknown data!")
        self.response_code = 400

    def get(self):
        '''
        http get方法用来查找设备历史记录信息，输出信息遵循 logs_response_marshaller 格式

        :param per: http get 参数，并不是函数传入参数，每页历史记录数量
        :param token: http get 参数，并不是函数传入参数，用户认证令牌
        :param topic: http get 参数，并不是函数传入参数，设备 topic 可以认为是设备唯一标示
        :param page: http get 参数，并不是函数传入参数， 与返回 page 参数不同，一定注意，这里表示请求哪一页数据
        :return:  json 化 设备历史记录信息

        状态码：
            200 OK - [GET]：服务器成功返回用户请求的数据，该操作是幂等的（Idempotent）

            401 授权失败，token 不可用

            404 设备历史记录信息不存在
        '''
        session = Session()
        per_page = request.args.get('per')
        token = request.args.get('token')
        topic = request.args.get('topic')
        page = request.args.get('page')
        user_info = token_auth.check_auth(token)
        if not user_info:
            self.response_data = self.response_handler([], 1, "Invalid token!")
            self.response_code = 401
        elif not per_page or not topic or not page:
            self.response_data = self.response_handler([], 1, "You must fill form correctly!")
            self.response_code = 401
        else:
            try:
                per_page = int(request.args.get('per'))
                page = int(request.args.get('page'))
                count = session.query(m_log.DeviceLog).filter_by(topic=topic).count()
                # 使用偏移量和数量限制来实现分页效果
                if page <= 0 or count < (page - 1) * per_page:
                    logs = []
                else:
                    logs = session.query(m_log.DeviceLog).filter_by(topic=topic).offset((page - 1) * per_page) \
                        .limit(per_page).all()
                try:
                    self.response_data = self.response_handler(logs, 0, "Get device logs success!",
                                                               int(count / per_page + 1), page)
                    self.response_code = 200
                except IndexError:
                    self.response_data = self.response_handler([], 1, "This device has no log!", 0, 0)
                    self.response_code = 404
            except ValueError:
                self.response_data = self.response_handler([], 1, "Parameter value error in your form!")
                self.response_code = 401
        session.close()
        return self.response_data, self.response_code

    @staticmethod
    def response_handler(logs, success, message, page=1, current_page=1):
        '''
        请求响应数据工厂，把数据库中查询得到的数据进行加工，处理成为 json 标准响应数据

        :param logs: 设备历史记录
        :param success: 此次查询请求结果
        :param message: 此次查询附加信息
        :param page: 历史记录总页数
        :param current_page: 历史记录当前页码数
        :return: 返回 logs_response_marshaller 格式的 json 数据
        '''
        logs_list = marshal(logs, logs_marshaller)
        logs_response_marshaller['data'] = logs_list
        logs_response_marshaller['success'] = success
        logs_response_marshaller['msg'] = message
        logs_response_marshaller['page'] = page
        logs_response_marshaller['current_page'] = current_page
        return logs_response_marshaller


def on_connect(client, userdata, flags, rc):
    '''
    服务器连接 mqtt 通信组件服务器成功回调函数

    :param client:  mqtt 客户端对象
    :param userdata:  mqtt 用户信息
    :param flags:
    :param rc:  连接状态码
    :return: 连接成功之后订阅相应消息，不返回数据
    '''
    client.subscribe(config.MQTT_TOPIC)


def on_message(client, userdata, msg):
    '''
    接收到 mqtt 消息回调函数

    :param client:  mqtt 客户端对象
    :param userdata:   mqtt 客户端对象
    :param msg:  mqtt 消息对象
    :return: 解析、存储消息到数据库中，不返回数据
    '''
    session = Session()
    try:
        json_data = json.loads(msg.payload)
    except JSONDecodeError:
        json_data = None
    if json_data:
        try:
            log = m_log.DeviceLog(topic=msg.topic, value=json_data['data'], date_time=datetime.datetime.utcnow())
            session.add(log)
            session.commit()
        except:
            pass
    session.close()


def on_disconnect(client, userdata, rc):
    '''
    连接断开事件，在这里重连

    :param client:   mqtt 客户端对象
    :param userdata:   mqtt 客户端对象
    :param rc: mqtt 连接返回的状态码
    :return: 不返回数据
    '''
    if rc != 0:
        init_log_queue()


def init_log_queue():
    '''
    初始化 mqtt 组件， 在 app 被初始化的时候调用

    :return: 不返回数据
    '''
    client = mqtt.Client()
    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.connect(config.MQTT_BROKER_URL, int(config.MQTT_BROKER_PORT), int(config.MQTT_KEEPALIVE))
    client.loop_start()
