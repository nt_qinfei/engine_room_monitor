from datetime import datetime, timedelta
import base64
import json
import config
from app.controller import Session
from app.models import user as muser
from binascii import Error


def generate_auth_token(username):
    '''
    产生 token 的函数

    :param username: 用户名
    :return: base64 编码的 token 信息，解码之后会得到登录用户名和过期时间
    '''
    payload = {
        'username': username,
        'exp': (datetime.utcnow() + timedelta(seconds=int(config.JWT_EXP_DELTA_SECONDS))).strftime("%Y-%m-%d %H:%M:%S"),
    }
    token = base64.urlsafe_b64encode(json.dumps(payload).encode("utf-8"))
    return token.decode()


def check_auth(token):
    '''
    验证 token 的函数

    :param token: token
    :return: 如果 token 有效返回 user 对象, 无效则返回  None
    '''

    try:
        token = base64.urlsafe_b64decode(token.encode("utf-8"))
    except (AttributeError, TypeError, Error):
        return None
    session = Session()
    try:
        json_token = json.loads(token)
        username = json_token['username']
        user = session.query(muser.User).filter_by(username=username).first()
        time = datetime.strptime(json_token['exp'], "%Y-%m-%d %H:%M:%S")
        if user and user.username == username and datetime.utcnow() < time:
            result = user
        else:
            result = None
    except json.decoder.JSONDecodeError:
        result = None
    session.close()
    return result


def check_token(token):
    '''
    验证 token 实现函数， 实际调用是这一个，不是上面 check_auth(token)

    :param token: token 令牌
    :return: 成功返回 User 对象， 失败返回 None
    '''
    if token:
        user_info = check_auth(token)
        return user_info
    else:
        return None
