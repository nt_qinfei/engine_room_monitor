from flask_restful import Resource, fields, marshal
from flask import request
from app.controller import Session
from app.models import device as m_device
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import UnmappedInstanceError
from app.controller import token_auth

# 预定义了返回数据的格式
device_marshaller = {
    'device_name': fields.String,
    'topic': fields.String,
    'value': fields.String,
    'remarks': fields.String
}

# 预定义了返回数据的格式
devices_marshaller = {
    'success': fields.Integer,
    'msg': fields.String,
    'data': fields.String
}


class DeviceResource(Resource):
    def __init__(self):
        self.response_data = self.response_handler([], 1, "Unknown data!")
        self.response_code = 400

    def get(self):
        '''
        http get方法用来查找设备列表，输出信息遵循 devices_marshaller 格式

        :param topic: http get 参数，并不是函数传入参数，设备 topic 可以认为是设备唯一标示, token 为空的时候直接返回所有设备列表
        :param token: http get 参数，并不是函数传入参数，用户认证令牌
        :return:  json 化 设备历史记录信息

        状态码：
            200 OK - [GET]：服务器成功返回用户请求的数据

            401 授权失败，token 不可用

            404 设备不存在
        '''
        session = Session()
        topic = request.args.get('topic')
        token = request.args.get('token')
        user_info = token_auth.check_token(token)
        if not user_info:
            self.response_data = self.response_handler([], 1, "Invalid token!")
            self.response_code = 401
        else:
            if topic:
                devices = session.query(m_device.Device).filter_by(topic=topic, username=user_info.username).all()
                # 后缀 .all() 返回了 list 没有后缀则返回 Query 对象
            else:
                devices = session.query(m_device.Device).filter_by(username=user_info.username).all()
            try:
                self.response_data = self.response_handler(devices, 0, "Get device list success!")
                self.response_code = 200
            except IndexError:
                self.response_data = self.response_handler([], 1, "Device not exists!")
                self.response_code = 404
        session.close()
        return self.response_data, self.response_code

    def post(self):
        '''
        http post 方法用来添加设备，输出信息遵循 devices_marshaller 格式

        :param device_name: http post 参数，设备名称
        :param token: http post 参数，并不是函数传入参数，用户认证令牌
        :param topic: http post 参数，并不是函数传入参数，设备 topic 可以认为是设备唯一标示
        :param remarks: http post 参数，并不是函数传入参数， 设备备注信息
        :return:  json 化 设备历史记录信息

        状态码：
            201 OK - [POST]：用户新建或修改数据成功

            400 用户发出的请求有错误，服务器没有进行新建或修改数据的操作

            401 授权失败，token 不可用

            404 设备历史记录信息不存在
        '''
        session = Session()
        device_name = request.values.get('device_name')
        topic = request.values.get('topic')
        remarks = request.values.get('remarks')
        token = request.values.get('token')
        user_info = token_auth.check_token(token)
        if not user_info:
            self.response_data = self.response_handler([], 1, "Invalid token!")
            self.response_code = 401
            session.close()
            return self.response_data, self.response_code
        device = session.query(m_device.Device).filter_by(topic=topic).first()
        if device:
            self.response_data = self.response_handler([], 1, "Topic has been used!")
            self.response_code = 404
        else:
            if not topic or not device_name:
                self.response_data = self.response_handler([], 1, "You must fill form correctly!")
                self.response_code = 400
            else:
                new_device = m_device.Device(username=user_info.username, device_name=device_name, topic=topic,
                                             remarks=remarks)
                try:
                    session.add(new_device)
                    session.commit()
                    self.response_data = self.response_handler([new_device], 0, "Insert device success!")
                    self.response_code = 201
                except IntegrityError:
                    self.response_data = self.response_handler([], 1, "Database error!")
                    self.response_code = 400
        session.close()
        return self.response_data, self.response_code

    def delete(self):
        '''
        http delete 方法用来删除设备，输出信息遵循 devices_marshaller 格式

        :param token: http delete 参数，并不是函数传入参数，用户认证令牌
        :param topic: http delete 参数，并不是函数传入参数，设备 topic 可以认为是设备唯一标示
        :return:  json 化 被删除的设备信息

        状态码：
            410 用户请求的资源被永久删除，且不会再得到，因为 204 会返回 null， 所以选择 410

            401 授权失败，token 不可用

            404 设备不存在
        '''
        session = Session()
        topic = request.values.get('topic')
        token = request.args.get('token')
        user_info = token_auth.check_token(token)
        if not user_info:
            self.response_data = self.response_handler([], 1, "Invalid token!")
            self.response_code = 401
            session.close()
            return self.response_data, self.response_code
        if not topic:
            self.response_data = self.response_handler([], 1, "Topic can not be null!")
            self.response_code = 404
        else:
            device = session.query(m_device.Device).filter_by(topic=topic, username=user_info.username).first()
            try:
                session.delete(device)
                session.commit()
                self.response_data = self.response_handler([], 0, "Delete device success!")
                self.response_code = 410
            except UnmappedInstanceError:
                session.rollback()
                self.response_data = self.response_handler([], 1, "Device not exists!")
                self.response_code = 400
        session.close()
        return self.response_data, self.response_code

    @staticmethod
    def response_handler(devices, success, message):
        '''
        请求响应数据工厂，把数据库中查询得到的数据进行加工，处理成为 json 标准响应数据

        :param devices: 设备列表
        :param success: 此次查询请求结果
        :param message: 此次查询附加信息
        :return: 返回 devices_marshaller 格式的 json 数据
        '''
        device_list = marshal(devices, device_marshaller)
        devices_marshaller['data'] = device_list
        devices_marshaller['success'] = success
        devices_marshaller['msg'] = message
        return devices_marshaller
