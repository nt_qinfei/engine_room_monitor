from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from flask import Flask
from sqlalchemy import exc
from sqlalchemy import event
from sqlalchemy import select

app = Flask(__name__)
app.config.from_object('config')
engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'], pool_recycle=3600)
Session = sessionmaker(bind=engine)
# session = Session()


@event.listens_for(engine, "engine_connect")
def ping_connection(connection, branch):
    '''
    数据库 session 连接检测事件， 断开之后在这里重新连接

    :param connection: session 连接对象
    :param branch: 数据库连接的子连接
    :return: 不返回数据
    '''

    if branch:
        return
    try:
        connection.scalar(select([1]))
    except exc.DBAPIError as err:
        if err.connection_invalidated:
            connection.scalar(select([1]))
        else:
            raise
