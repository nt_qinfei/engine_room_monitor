# -*- encoding:utf-8 -*-
from sqlalchemy.exc import IntegrityError
from app.controller import Session
from flask_restful import fields, Resource
from app.models import user as m_user
from flask import request
from flask_login import login_user
from app.controller import token_auth

# 使用 marshal_with 装饰， 输出信息做蒙版，来屏蔽多余信息， 添加缺少的信息为空，并保持输出数据类型一致性
user_marshaller = {
    'success': fields.Integer,
    'msg': fields.String,
    'data': fields.String
}

# 使用 marshal_with 装饰， 输出信息做蒙版，来屏蔽多余信息， 添加缺少的信息为空，并保持输出数据类型一致性
login_marshaller = {
    'success': fields.Integer,
    'msg': fields.String,
    'data': fields.String
}


class UserResource(Resource):
    '''
    用户资源HTTP方法

        GET 获取用户信息

        POST 添加用户
    '''
    def __init__(self):
        self.user_marshaller = user_marshaller
        self.response_code = 400

    def get(self):
        '''
        http get方法用来查找用户信息，使用 user_fields 装饰

        :return:  json 化 user 表，包含用户 id 和 username

        状态码：
            200 OK - [GET]：服务器成功返回用户请求的数据，该操作是幂等的（Idempotent）

            404 用户不存在
        '''
        session = Session()
        username = request.args.get('username')
        user = session.query(m_user.User).filter_by(username=username).first()
        if user and username:
            self.user_marshaller['success'] = 0
            self.user_marshaller['msg'] = 'Get user ok!'
            self.user_marshaller['data'] = {'username': username}
            self.response_code = 200
        else:
            self.user_marshaller['success'] = 1
            self.user_marshaller['msg'] = 'user not exists!'
            self.user_marshaller['data'] = {'username': ''}
            self.response_code = 404
        session.close()
        return self.user_marshaller, self.response_code

    def post(self):
        '''
        http post 方法，添加用户， 不使用 user_fields 装饰， 返回信息格式自由

        :return: json 化插入用户的结果，包含 success 代码，和原因

        success 代码 0：用户插入成功， 1：用户已经存在，2：数据库错误

        状态码：
            201 [POST/PUT/PATCH]：用户新建或修改数据成功。

            400 用户发出的请求有错误，服务器没有进行新建或修改数据的操作，该操作是幂等的。
        '''
        session = Session()
        username = request.values.get('username')
        password = request.values.get('password')
        if not username or not password:
            self.user_marshaller['success'] = 1
            self.user_marshaller['msg'] = 'Username or password can not be null!'
            self.user_marshaller['data'] = {'username': ''}
            self.response_code = 400
        else:
            user = session.query(m_user.User).filter_by(username=username).first()
            if user:
                self.user_marshaller['success'] = 1
                self.user_marshaller['msg'] = 'User already exists!'
                self.user_marshaller['data'] = {'username': username}
                self.response_code = 400
            else:
                new_user = m_user.User(username=username, password=password)
                try:
                    session.add(new_user)
                    session.commit()
                    self.user_marshaller['success'] = 0
                    self.user_marshaller['msg'] = 'User insert ok!'
                    self.user_marshaller['data'] = {'username': username}
                    self.response_code = 201
                except IntegrityError:
                    self.user_marshaller['success'] = 1
                    self.user_marshaller['msg'] = 'Database error!'
                    self.user_marshaller['data'] = {'username': ''}
                    self.response_code = 400
        session.close()
        return self.user_marshaller, self.response_code


class UserAuth(Resource):
    '''
    用户认证类， 负责验证密码并生成token并返回给用户
    '''

    def __init__(self):
        self.login_marshaller = login_marshaller
        self.response_code = 400

    def post(self):
        '''
        提交用户名密码换取token函数，需要参数 用户名，密码

        .. code-block:: python

            url: /api/user/get_token

        :return: 使用 login_marshaller 装饰的格式
        '''
        session = Session()
        username = request.values.get('username')
        password = request.values.get('password')
        user = session.query(m_user.User).filter_by(username=username).first()
        if not user or not username:
            self.login_marshaller['success'] = 1
            self.login_marshaller['msg'] = 'User not exists!'
            self.login_marshaller['data'] = {'token': ''}
            self.response_code = 404
        else:
            if not password or not user.verify_password(password=password):
                self.login_marshaller['success'] = 1
                self.login_marshaller['msg'] = 'User authorize failed!'
                self.login_marshaller['data'] = {'token': ''}
                self.response_code = 401
            else:
                login_user(user)
                self.login_marshaller['success'] = 0
                self.login_marshaller['msg'] = 'User authorize success!'
                token = token_auth.generate_auth_token(username)
                self.login_marshaller['data'] = {'token': token}
                self.response_code = 200
        session.close()
        return self.login_marshaller, self.response_code
