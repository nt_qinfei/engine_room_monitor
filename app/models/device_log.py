# -*- encoding:utf-8 -*-
from sqlalchemy import Column, Integer, String, DateTime
from app.models import Base
import datetime


class DeviceLog(Base):
    '''
    设备历史记录数据表节构

    ==========   =============   ================================
    Parameter    Type            Description
    ==========   =============   ================================
    id           Integer         数据库唯一 id
    topic        String(16)      设备唯一 id MQTT 话题
    value        String(64)      设备 value 数值， 默认为0
    date_time    DateTime        设备创建时间/数据更新时间
    ==========   =============   ================================
    '''
    __tablename__ = 'device_log'
    id = Column(Integer, primary_key=True)
    topic = Column(String(64), nullable=False)
    value = Column(String(64), nullable=False)
    date_time = Column(DateTime, nullable=False)

    def __init__(self, topic, value="0", date_time=datetime.datetime.utcnow()):
        self.topic = topic
        self.value = value
        self.date_time = date_time
