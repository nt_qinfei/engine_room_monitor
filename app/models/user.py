# -*- encoding:utf-8 -*-
from sqlalchemy import Column, Integer, String
from werkzeug.security import generate_password_hash, check_password_hash
from app.models import Base
from sqlalchemy.orm import relationship


class User(Base):
    '''
    用户数据表节构

    =============    =============   ================================
    Parameter        Type            Description
    =============    =============   ================================
    id               Integer         数据库唯一 id
    username         String(128)     用户名
    password_hash    String(128)     用户密码 hash 值
    =============    =============   ================================
    '''
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(128), unique=True, nullable=False)
    password_hash = Column(String(128), nullable=False)

    device = relationship("Device", backref="user")

    def __init__(self, username, password):
        self.username = username
        self.hash_password(password)

    def hash_password(self, password):
        '''
        对用户注册时候使用的密码进行单项hash加密，并转存为password_hash字段

        :param password: 用户密码明码
        :return: 不返回信息，直接修改用户提交的password到password_hash字段
        '''
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        '''
        验证用户密码,把用户输入的密码做hash并与数据库中的密码进行比较

        :param password: 用户密码
        :return: True/False
        '''
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def is_authenticated():
        return True

    @staticmethod
    def is_active():
        return True

    @staticmethod
    def is_anonymous():
        return False

    def get_id(self):
        return self.id

