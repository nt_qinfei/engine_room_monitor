# -*- encoding:utf-8 -*-
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from app.models import Base
import datetime


class Device(Base):
    '''
    设备数据表节构

    ============   =============   ================================
    Parameter      Type            Description
    ============   =============   ================================
    id             Integer         数据库唯一id
    username       String(128)     用户名
    device_name    String(32)      设备名称
    topic          String(16)      设备唯一id,MQTT话题
    value          String(64)      设备 value 数值,默认为0(预留)
    remarks        String(256)     设备备注
    date_time      DateTime        设备创建时间/数据更新时间(预留)
    ============   =============   ================================
    '''
    __tablename__ = 'device'
    id = Column(Integer, primary_key=True)
    username = Column(String(128), ForeignKey('user.username'), nullable=False)
    device_name = Column(String(128), nullable=True)
    topic = Column(String(64), nullable=False, unique=True)
    value = Column(String(64), nullable=False)
    remarks = Column(String(256), nullable=True)
    date_time = Column(DateTime, nullable=False, default=datetime.datetime.utcnow())

    def __init__(self, username, device_name, topic, remarks):
        self.username = username
        self.device_name = device_name
        self.topic = topic
        self.value = "0"
        self.remarks = remarks
