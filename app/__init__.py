# -*- encoding:utf-8 -*-
from flask import Flask, g
import config
from flask_login import current_user
from flask_login import LoginManager
from flask_restful import Api
# 这里导入 modal 各个模块是为了初始化数据库表
from app.models import Base
from app.models import user as m_user
from app.models import device as m_device
from app.models import device_log as m_log
from app.controller.user import UserResource, UserAuth
from app.controller.device import DeviceResource
from app.controller.device_log import DeviceLogResource
import app.controller.device_log as c_log
from app.controller import Session, engine

app = Flask(__name__)
app.config.from_object(config)
api = Api(app)
login_manager = LoginManager()
login_manager.init_app(app)

api.add_resource(UserResource, '/user')
api.add_resource(UserAuth, '/login')
api.add_resource(DeviceResource, '/device')
api.add_resource(DeviceLogResource, '/log')
'''
上面几句在应用中注册了 flask-restful 的 url 信息，访问对应 url 即可对相应资源进行操作
'''


@app.before_request
def before_request():
    '''
    对用户每一次访问进行鉴权，此应用中是基于 token 令牌验证

    :return: 不返回数据
    '''
    g.user = current_user


@login_manager.unauthorized_handler
def unauthorized():
    '''
    对用户鉴权之后未经授权的用户会返回如下信息

    :return: 返回 401 数据

    .. code-block:: python

        {"user": "None", "success": 0}
    '''
    return {"user": "None", "success": 0}, 401


@login_manager.user_loader
def load_user(user_id):
    '''
    登录成功回调函数

    :param user_id: 传入 用户 id
    :return: 返回 User 类
    '''
    session = Session()
    data = session.query(m_user.User).get(user_id)
    session.close()
    return data


'''
程序开始运行的时候，会默认运行此文件代码

c_log.init_log_queue()，启动 mqtt 通信系统连接服务器并对所有 mqtt 通信信息进行监听，并进行相应处理
Base.metadata.create_all(engine)， 创建应用所需数据表，如果数据表已经存在不进行任何操作
'''
c_log.init_log_queue()
Base.metadata.create_all(engine)
