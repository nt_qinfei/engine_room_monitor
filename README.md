<h1 align="center">机房监控项目</h1>

工程使用Flask框架

使用python3.6开发

工程依赖软件包在 requirements.txt 里面

### 使用方法

* 创建基于 Python3.6 的 virtualenv 虚拟环境
* 激活环境之后运行 pip install -r requirements.txt 安装工程所需依赖软件包
* 修改config.py 里面 SQLALCHEMY_DATABASE_URI 字段数据库信息，并保证用户对于数据库足够权限
* ~~运行 app/\_\_init\_\_.py 初始化数据库~~  现在每次启动都会创建数据表
* 测试环境：运行 python manage.py 开始运行 （调试运行）
* 基于 Python WSGI 部署：运行 bash run_gunicorn.sh 即可访问 服务器 ip:5000(在脚本中自行更改端口)（调试运行）
* 基于 Docker 部署，请自行编译Dockerfile， 并部署 (生产环境可用)
* Nginx 反向代理请自行配置 (生产环境可用)

程序文档参见 ： [``` http://damoyelang1992.oschina.io/engine_room_monitor/document/html/index.html ```](http://damoyelang1992.oschina.io/engine_room_monitor/document/html/index.html)

方法文档参见 ./document/*.md 