FROM python:3.6.1

RUN  mkdir /root/.pip
COPY ./pip.conf    /root/.pip/pip.conf
COPY ./requirements.txt    /opt/requirements.txt
COPY ./run_gunicorn.sh    /run_gunicorn.sh

RUN  chmod 755 /run_gunicorn.sh
RUN  pip install -r /opt/requirements.txt

# 这里是在Docker 虚拟机里面创建的路径,在 Docker 指令中应该映射代码到此目录下
VOLUME ["/code"]

EXPOSE 5000

CMD  /run_gunicorn.sh