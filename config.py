# -*- encoding:utf-8 -*-
import os
CSRF_ENABLED = True

# 以下是时事通信系统 mqtt 的配置信息，请勿泄漏，可以更改为自己的服务器配置
MQTT_BROKER_URL = os.environ["MQTT_BROKER_URL"]
# use the free broker from HIVEMQ
MQTT_BROKER_PORT = os.environ["MQTT_BROKER_PORT"]
# default port for non-tls connection
MQTT_USERNAME = ''
# set the username here if you need authentication for the broker
MQTT_PASSWORD = ''
# set the password here if the broker demands authentication
MQTT_KEEPALIVE = 60
# set the time interval for sending a ping to the broker to 5 seconds
MQTT_TLS_ENABLED = False
# set TLS to disabled for testing purposes
# mqtt 主题，每一个主题对应一个传感器，所有主题格式为： er/ 开头 + esp8266的 chip_id() + Zigbee CC2530 的 chip_id + 传感器编号（1位）
MQTT_TOPIC = os.environ["MQTT_TOPIC"]

JWT_EXP_DELTA_SECONDS = os.environ["JWT_EXP_DELTA_SECONDS"]
SECRET_KEY = os.environ["SECRET_KEY"]
MYSQL_HOST = os.environ["MYSQL_HOST"]
MYSQL_PORT = os.environ["MYSQL_PORT"]
MYSQL_USERNAME = os.environ["MYSQL_USERNAME"]
MYSQL_PASSWORD = os.environ["MYSQL_PASSWORD"]
MYSQL_DBNAME = os.environ["MYSQL_DBNAME"]
DEBUG_HOST_ADDR = os.environ["DEBUG_HOST"]

SQLALCHEMY_DATABASE_URI = ('mysql+mysqldb://' + MYSQL_USERNAME + ':' + MYSQL_PASSWORD + '@' + MYSQL_HOST + ':'
                           + MYSQL_PORT + '/' + MYSQL_DBNAME + '?charset=utf8')


if __name__ == "__main__":
    print(os.environ["MQTT_BROKER_URL"])
